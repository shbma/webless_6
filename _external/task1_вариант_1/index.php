<?php
$wanted = isset($_GET['page']) ? $_GET['page'] : 'about'; //страница

$content_names = array( //названия меню
	'about' => 'О нас',
	'contacts' => 'Контакты',
	'products' => 'Продукты',
	'news' => 'Новости',
	'stuff' => 'Персонал');
$ext = '.txt';

if (in_array($wanted,array_keys($content_names)) && ($text = file_get_contents('content/'.$wanted.$ext))) {
		
	$main = file_get_contents('templates/main.tpl');//основной шаблон
	$main = str_replace('{CONTENT}',$text,$main);
	
	$menu = '';	
	foreach($content_names as $m_key => $m_name){ //меню		
		$link_class = ($m_key != $wanted) ? array('<a href="index.php?page='.$m_key.'">','</a>') : array('','');
		$menu .= '<li>'.$link_class[0].$content_names[$m_key].$link_class[1].'</li>';		
		}
	
	$main = str_replace('{MENU}',$menu,$main);
	
	echo $main;
	}

?>
