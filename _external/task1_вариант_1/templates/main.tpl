<html>

<head>
<title>Ресторан "О вкусах не спорят"</title>
<meta charset='utf-8'>
<link type="text/css" rel="stylesheet" href="templates/style.css">
</head>

<body>

<header> <h2>Ресторан "О вкусах не спорят"</h2> </header>

<nav> <ul>{MENU}</ul> </nav>

<div class="content"> {CONTENT}</div>

<footer> 
<div class="copyright">Разработано в Веселой компании.</div>
</footer>

</body>
</html>
