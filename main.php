<?php
$dir = opendir ($content_path = __DIR__."/content/");

//формируем массив возможных имен страниц - из папок каталога content
$content_names = array();
while ($file = readdir ($dir)) {
      if ( is_dir($content_path.$file) && $content_path.$file !== '.' && $content_path.$file !== '..'
              && is_file($content_path.$file.'/h1.txt')) {
         $content_names[$file] = file_get_contents($content_path.$file.'/h1.txt');           
      }
}
closedir ($dir);

$wanted = isset($_GET['page']) ? $_GET['page'] : 'about'; //запрошенная страница

//формируем меню
$menu = '';
foreach ($content_names as $key => $value) {
    $menu .= $key.'#'.$value.'#'.($key === $wanted ? 'current_li' : 'normal_li').';';
}

$ext = '.txt';

//выдаем результат, если все условия в идеальном порядке
if (in_array($wanted,array_keys($content_names))) { 
    $text = file_get_contents($content_path.$wanted.'/'.'content'.$ext);
    $title = file_get_contents($content_path.$wanted.'/'.'title'.$ext);
    $h1 = file_get_contents($content_path.$wanted.'/'.'h1'.$ext);

    if ( ($text !== FALSE) && ($title !== FALSE) && ($h1 !== FALSE) )
        echo $text.'<->'.$title.'<->'.$h1.'<->'.$menu;		
}

?>
