<?php
$wanted = isset($_GET['page']) ? $_GET['page'] : 'about'; //страница

$content_names = array( //названия меню
	'about' => 'О нас',
	'contacts' => 'Контакты',
	'products' => 'Продукты',
	'news' => 'Новости',
	'stuff' => 'Персонал');
$ext = '.txt';

if (in_array($wanted,array_keys($content_names)) && ($text = file_get_contents('content/'.$wanted.$ext))) {		
		echo $text;	
}

?>
