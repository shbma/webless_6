﻿$(document).ready(function(){	
	
	var base_path = document.location.pathname.replace(/\w+\.html/g,''); //корень сайта	
	
        function make_menu(menu,container_tags,item_tags){ 
            //формируем меню из строки ключ#название#текущая_или_нет
            console.log(menu);
            res = container_tags[0];
            m_blocks = menu.split(';');
            for(i = 0; i < m_blocks.length-1; i++ ){
                var m_parts = m_blocks[i].split('#');
				m_parts[1] = (m_parts[2] == 'current_li') ? m_parts[1].toUpperCase() : m_parts[1];
                res += '<li id="'+m_parts[0]+'" class="'+m_parts[2]+'">'+m_parts[1]+'</li>';
            }
            res += container_tags[1];
            return res;
        }
                
	function take_request(){ // разбираем GET-запрос, возвращаем объект с GET-параметрами
						
		var get = location.search;  // строка GET запроса
		var param = {}; // параметры в удобном виде
		console.log('get= '+get);
		
		if(get != '') {
			var get_pairs = (get.substr(1)).split('&');   // разделяем переменные
			//console.log('get2= '+get_pairs);			
			for(i in get_pairs) {
				var pair = get_pairs[i].split('=');       				
				param[pair[0]] = pair[1];       // пары ключ(имя переменной)->значение				
			}		
		}
		return param;
	}
	
	function ask_php(where,is_admin){ //загрузка произвольной страницы
		console.log('='+where);
		request_data = {'page':where}
		if (is_admin) { // есть параметр админского входа
			request_data['admin'] = ''
		}
		$.ajax({
			type: 'GET',
			url: base_path+'main.php',
			data: request_data,
			success: function(msg){
				//console.log(msg);
				msg_arr = msg.split('<->');
				$('title').html(msg_arr[1]);
				$('h1:first').html(msg_arr[2]);
                                
                                $('nav').html(make_menu(msg_arr[3],
                                                Array('<ul>','</ul>'),
                                                Array('<li>','</li>')));
                                                
                                $('nav li').each(function(){ //оживляем пункты меню
                                    $(this).click(function(){

                                            //меняем url
                                            history.pushState({}, '', base_path+'index.html?page='+$(this).attr('id'));
                                            extended_ask_php();
                                    });
                                });
                                
				if (!is_admin) {
					$('.content').html(msg_arr[0]);
				} else {					
					//$('iframe').contents().find('body.content').html(msg)
					$('.admin_content').css('display','block');
					$('#content_editor').html(msg_arr[0]);
					
					$('#title_editor').html(msg_arr[1]);
					$('#h1_editor').html(msg_arr[2]);
				}
				
			}
		});
	}
	
	function extended_ask_php(init){ //запрос к серверу исходя из текущих GET-параметров
		//console.log(take_request());
		
		var par_get = take_request(); // берем GET-параметры
		
		var default_page = 'about'; // страница по умолчанию
		if (par_get.page) default_page = par_get.page;
		
		var try_admin = false; // хотим ли выполнить административный вход
		if (par_get.admin) try_admin = true;
		
		if (init) history.pushState({}, '', base_path+'index.html?page='+default_page); // если входим 1й раз - без GET параметров
		
		ask_php(default_page,try_admin); //начальная страница
	}
	
	extended_ask_php(); // рисуем начальную страницу		
	
	CKEDITOR.replace( 'content_editor',{});  // натравливаем wysiwyg на элемент с заданным id
	
	window.onpopstate = function( e ) { // для кнопки Назад браузера
		
		e.preventDefault(); //отмена действия браузера по умолчанию		
		extended_ask_php();	//подгружаем содержимое	ориентируясь на GET-параметры
	}

	
	$('#save').click(function(){ //обработка кнопки Сохранить
			
		console.log('saving');

		//формируем простые параметры для Сохраняющего скрипта
		var request_data = {'save':'yes',
						'title':$('#title_editor').val(),
						'h1':$('#h1_editor').val()};
		
		//не проверено
		if (the_page = take_request().page) {
                    request_data['page'] = the_page;
                    request_data['content'] = $('iframe').contents().find('body.content').html();
                }							
	
		$.ajax({
			type: 'GET',
			url: base_path+'save.php',
			data: request_data,
			success: function(msg){
				//console.log(msg);
				extended_ask_php();
				
			}
		});	
	});

	
	$('#edit').click(function(){ // ныряем в режим Админа
		/*history.pushState({}, '', window.location.toString()+'&admin=1');
		var req = take_request();
		ask_php( req.page ? req.page : 'about' ,true);*/
		window.location = window.location.toString()+'&admin=1';		
	});

	
});
