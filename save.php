<?php

if (isset($_GET['save'])) { // обновить данные
    echo 'save';

    $dir = opendir ($content_path = __DIR__."/content/");

    //формируем массив возможных имен страниц - из папок каталога content
    $content_names = array();
    while ($file = readdir ($dir)) {
          if ( is_dir($content_path.$file) && $content_path.$file !== '.' && $content_path.$file !== '..'
                  && is_file($content_path.$file.'/h1.txt')) {
             $content_names[$file] = file_get_contents($content_path.$file.'/h1.txt');           
          }
    }
    closedir ($dir);
    
    //перебираем страницы и сохраняем данные в подходящей
    foreach($content_names as $k => $v) {
        echo $k.' ';
        if ($_GET['page'] == $k) { //если инфа для данной стр. есть в запросе
            echo 'is';
            if (isset($_GET['content']))
                file_put_contents($content_path.$k.'/content.txt',$_GET['content']);
            if (isset($_GET['title'])) 
                file_put_contents($content_path.$k.'/title.txt',$_GET['title']);
            if (isset($_GET['h1'])) 
                file_put_contents($content_path.$k.'/h1.txt',$_GET['h1']);
        }
    }       	
};
?>
